const puppeteer = require('puppeteer');
let assert = require('assert');

(async () => {
        const browser = await puppeteer.launch({ headless: true, args: ['--start-maximized'] });
        const page = await browser.newPage();
    try{
        //setup puppeteer
        await page.setViewport({ width: 1920, height: 1080});

        //go to meteo.imgw
        await page.goto('https://meteo.imgw.pl/');

        //check current selected city
        await page.waitForSelector('#model-curr-city');
        const oldCityElement = await page.$('#model-curr-city');
        const oldCityText = await page.evaluate(element => element.textContent, oldCityElement);
        console.log("Aktualnie wybrane miasto to: " + oldCityText);

        //select new city
        await page.focus('#citysearch_model');
        await page.keyboard.type('Szczecin');
        await page.waitFor(2000);
        const elements = await page.$x("// div[@tabindex='-1'][contains(.,'Szczecin gm. Szczecin')]");
        await elements[0].click();

        //check new city
        await page.waitFor(2000);
        await page.waitForSelector('#model-curr-city');
        const currentCityElement = await page.$('#model-curr-city');
        const currentCityText = await page.evaluate(element => element.textContent, currentCityElement);
        console.log("Nowo wybrane miasto to: " + currentCityText);

        //check if city changed
        assert(oldCityText !== currentCityText);

        await browser.close();
    }catch (error){
        console.log(error);
    }
})();